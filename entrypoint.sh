#!/bin/bash -e

if [ -f "/opt/app/activity.dat" ] ; then
	echo "Removing activity.dat"
    rm "/opt/app/activity.dat"
fi
if [ -f "/opt/app/peers.ixi" ] ; then
	echo "Removing peers.dat"
    rm "/opt/app/peers.ixi"
fi
if [ -f "/opt/app/testnet-activity.dat" ] ; then
	echo "Removing testnet-activity.dat"
    rm "/opt/app/testnet-activity.dat"
fi
if [ -f "/opt/app/testnet-peers.ixi" ] ; then
	echo "Removing testnet-peers.ixi"
    rm "/opt/app/testnet-peers.ixi"
fi

echo "Starting IxianDLT"
mono IxianDLT.exe --config /opt/app/ixian.cfg --verboseOutput --walletPassword $WALLET_PASSWORD "$@"
