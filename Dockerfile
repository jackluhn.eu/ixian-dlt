# Define base image
FROM mono:latest AS build-env
RUN apt update  && \
    apt install -y ca-certificates-mono make gcc libargon2-0

COPY ["./Ixian-Core/", "/source/Ixian-Core/"]
COPY ["./Ixian-DLT/", "/source/Ixian-DLT/"]

RUN cd /source/Ixian-DLT/ && \
    nuget restore DLTNode.sln && \
    msbuild DLTNode.sln /p:Configuration=Release /target:DLTNode

RUN cp /usr/lib/x86_64-linux-gnu/libargon2.so.0 /source/Ixian-DLT/IxianDLT/bin/Release/libargon2.so

FROM mono:latest
COPY --from=build-env /source/Ixian-DLT/IxianDLT/bin/Release/ /opt/app/
COPY ./entrypoint.sh /opt/entrypoint.sh
WORKDIR /opt/app
ENTRYPOINT ["/opt/entrypoint.sh"]
